package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVEatBeans extends LVBase implements Component.DrawTask {

    private Paint mPaint, mPaintEye;
    private float mWidth = 0f;
    private float mHigh = 0f;
    private float mPadding = 5f;
    private float eatErWidth = 60f;
    private float eatErPositonX = 0f;
    int eatSpeed = 5;
    private float beansWidth = 10f;
    private float mAngle = 34;
    private float eatErStrtAngle = mAngle;
    private float eatErEndAngle = 360 - 2 * eatErStrtAngle;

    public LVEatBeans(Context context) {
        this(context, null);
    }

    public LVEatBeans(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVEatBeans(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void InitPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(Color.WHITE);

        mPaintEye = new Paint();
        mPaintEye.setAntiAlias(true);
        mPaintEye.setStyle(Paint.Style.FILL_STYLE);
        mPaintEye.setColor(Color.BLACK);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mWidth = getWidth();
        mHigh = getHeight();
        float eatRightX = mPadding + eatErWidth + eatErPositonX;
        RectFloat rectF = new RectFloat(mPadding + eatErPositonX, mHigh / 2 - eatErWidth / 2, eatRightX, mHigh / 2 + eatErWidth / 2);
        canvas.drawArc(rectF, new Arc(eatErStrtAngle, eatErEndAngle, true), mPaint); // 是否显示半径
        canvas.drawCircle(mPadding + eatErPositonX + eatErWidth / 2, mHigh / 2 - eatErWidth / 4, beansWidth / 2, mPaintEye);
        int beansCount = (int) ((mWidth - mPadding * 2 - eatErWidth) / beansWidth / 2);
        for (int i = 0; i < beansCount; i++) {
            float x = beansCount * i + beansWidth / 2 + mPadding + eatErWidth;
            if (x > eatRightX) {
                canvas.drawCircle(x, mHigh / 2, beansWidth / 2, mPaint);
            }
        }
    }

    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        invalidate();
    }

    public void setEyeColor(int color) {
        mPaintEye.setColor(new Color(color));
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        float mAnimatedValue = value;
        eatErPositonX = (mWidth - 2 * mPadding - eatErWidth) * mAnimatedValue;
        eatErStrtAngle = mAngle * (1 - (mAnimatedValue * eatSpeed - (int) (mAnimatedValue * eatSpeed)));
        eatErEndAngle = 360 - eatErStrtAngle * 2;
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        eatErPositonX = 0;
        invalidate();
        return 1;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }

}
