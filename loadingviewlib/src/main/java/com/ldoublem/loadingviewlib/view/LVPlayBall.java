package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVPlayBall extends LVBase implements Component.DrawTask {

    private Paint mPaint, mPaintCircle, mPaintBall;
    private float mPaintStrokeWidth;
    private float mHigh = 0f;
    private float mWidth = 0f;
    private float mRadius;
    private float mRadiusBall;
    private float quadToStart = 0f;
    private float ballY = 0f;
    private boolean isFirst = true;
    Path path = new Path();

    public LVPlayBall(Context context) {
        this(context, null);
    }

    public LVPlayBall(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVPlayBall(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mRadius = dip2px(3);
        mPaintStrokeWidth = 2;
        mRadiusBall = dip2px(4);
    }

    @Override
    protected void InitPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);

        mPaintCircle = new Paint();
        mPaintCircle.setAntiAlias(true);
        mPaintCircle.setStyle(Paint.Style.STROKE_STYLE);
        mPaintCircle.setColor(Color.WHITE);

        mPaintBall = new Paint();
        mPaintBall.setAntiAlias(true);
        mPaintBall.setStyle(Paint.Style.FILL_STYLE);
        mPaintBall.setColor(Color.WHITE);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mWidth = getWidth();
        mHigh = getHeight();
        if (isFirst) {
            quadToStart = mHigh / 2;
            ballY = mHigh / 2;
            isFirst = false;
        }
        path = new Path();
        path.moveTo(0 + mRadius * 2 + mPaintStrokeWidth, mHigh / 2);
        path.quadTo(mWidth / 2, quadToStart, mWidth - mRadius * 2 - mPaintStrokeWidth, mHigh / 2);
        mPaint.setStrokeWidth(2);
        canvas.drawPath(path, mPaint);
        mPaintCircle.setStrokeWidth(mPaintStrokeWidth);
        canvas.drawCircle(mRadius + mPaintStrokeWidth, mHigh / 2, mRadius, mPaintCircle);
        canvas.drawCircle(mWidth - mRadius - mPaintStrokeWidth, mHigh / 2, mRadius, mPaintCircle);
        if (ballY - mRadiusBall > mRadiusBall) {
            canvas.drawCircle(mWidth / 2, ballY - mRadiusBall, mRadiusBall, mPaintBall);
        } else {
            canvas.drawCircle(mWidth / 2, mRadiusBall, mRadiusBall, mPaintBall);
        }
    }

    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        mPaintCircle.setColor(new Color(color));
        invalidate();
    }

    public void setBallColor(int color) {
        mPaintBall.setColor(new Color(color));
        invalidate();
    }


    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        if (value > 0.75) {
            quadToStart = mHigh / 2 - (1f - value) * mHigh / 3f;
        } else {
            quadToStart = mHigh / 2 + (1f - value) * mHigh / 3f;
        }
        if (value > 0.35f) {
            ballY = mHigh / 2 - (mHigh / 2 * value);
        } else {
            ballY = mHigh / 2 + (mHigh / 6 * value);
        }
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        quadToStart = mHigh / 2;
        ballY = mHigh / 2;
        return 0;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }

}
