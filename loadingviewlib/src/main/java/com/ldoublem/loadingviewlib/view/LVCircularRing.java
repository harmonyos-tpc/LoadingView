package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVCircularRing extends LVBase implements Component.DrawTask {

    private Paint mPaint;
    private Paint mPaintPro;
    private float mWidth = 0f;
    private float mPadding;
    private float startAngle = 0f;
    RectFloat rectF = new RectFloat();

    public LVCircularRing(Context context) {
        this(context, null);
    }

    public LVCircularRing(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVCircularRing(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPadding = 5;
    }

    @Override
    protected void InitPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(8);

        mPaintPro = new Paint();
        mPaintPro.setAntiAlias(true);
        mPaintPro.setStyle(Paint.Style.STROKE_STYLE);
        mPaintPro.setColor(new Color(0xA0FFFFFF));
        mPaintPro.setStrokeWidth(8);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2 - mPadding, mPaintPro);
        rectF = new RectFloat(mPadding, mPadding, mWidth - mPadding, mWidth - mPadding);
        canvas.drawArc(rectF, new Arc(startAngle, 100, false), mPaint); // 是否显示半径
    }


    public void setViewColor(int color) {
        mPaintPro.setColor(new Color(color));
        invalidate();
    }

    public void setBarColor(int color) {
        mPaint.setColor(new Color(color));
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        startAngle = 360 * value;
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        return 0;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }
}
